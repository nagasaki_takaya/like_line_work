<?php
class Validation {
    public $validation;
    public $userName;
    public $message;

    /**
     * バリデーションに必要な値をセットする
     *
     * @param array $datas array( 'uerName' => ユーザ名,
     *                            'message' => メッセージ );
     */
    public function set($data) {

        // ユーザ名
        if (isset($data['userName'])) {
            $this->userName = trim($data['userName']);
        }

        // メッセージ
        if (isset($data['message'])) {
            $this->message = trim($data['message']);
        }
    }

    public function overName() {
        $userNameInt = mb_strlen($this->userName);
        $decision = true;
        if ($userNameInt > 10) {
            $this->validation = "10文字以内で入力してください";
            $decision = false;
        } else {
            header("Location: ./chat.php");
        }
        return $decision;
    }
    public function noMessage() {
        $decision = true;
        if (empty($this->message) == true) {
            $this->validation = "メッセージが未入力です";
            $decision = false;
        }
        return $decision;
    }
    public function overMessage() {
        $messageInt = mb_strlen($this->message);
        $decision = true;
        if ($messageInt > 140) {
            $this->validation .= "140文字以内で入力してください";
            $decision = false;
        }
        return $decision;
    }
}
/* トークンがうまくいかないので後回し
    public function setToken() {
        $token = sha1(uniqid(mt_rand(), true));
        $_SESSION['token'] = $token;
    }
    public function checkToken() {
        if (empty($_SESSION['token']) == true || ($_SESSION['token'] != $_POST['token'])) {
            header("Location: ./index.php?error");
        }
    }
    public function h($s) {
        return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
    }
}
 */
?>