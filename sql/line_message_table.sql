-- like_line
-- line_messageテーブルの作成

create table line_message (
  id int(10) not null auto_increment primary key,
  session_id text not null,
  user_name text,
  message text not null,
  created datetime default now()
);