<?php
require_once __DIR__ . '/class/Validation.php';

//チャットページでセッションがなかったときのエラーメッセージ
function noSession($validation) {
    if (isset($_GET['error'])) {
        $validation = "エラーが発生しました";
    }
    return $validation;
}

$errorMessage = noSession("");

// ボタンが押された後の処理
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $userName = $_POST['user_name'];
    // ユーザー名に入力がなければムーン
    if (empty($userName) == true) {
        $userName = "ムーン";
    }
    // セッション開始
    session_start();

    if (isset($_SESSION['sessionId']) && $_SESSION['sessionId'] == "deleted") {
        session_regenerate_id();
    }

    $_SESSION['userName'] = $userName;
    $_SESSION['sessionId'] = session_id();

    // バリデーションの値を設定
    $getValidation = new Validation();
    $data = array(
        'userName' => $userName
    );
    $getValidation->set($data);

    if ($getValidation->overName() == false) {
        $errorMessage = $getValidation->validation;
    }
}

?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <title>like line</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <div class="chat_title">like line</div>
    </header>
    <div class="error_message_start_page">
<?php
if (empty($errorMessage) == false) {
    echo $errorMessage;
}
?>
    </div>
    <div class="sign_up_space">
        <div class="heading_name">
        Name:
        </div>
        <form action="index.php" method="post" class="name_form">
            <input type="text" name="user_name" class="name_write" size="13">
            <input type="submit" value="begin!" class="name_posting">
        </form>
    </div>
</body>
</html>